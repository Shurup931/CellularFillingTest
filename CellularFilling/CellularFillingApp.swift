//
//  CellularFillingApp.swift
//  CellularFilling
//
//  Created by Шарап Бамматов on 03.04.2024.
//

import SwiftUI

@main
struct CellularFillingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
